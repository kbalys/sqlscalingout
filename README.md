# SQL Server 2017 Scaling Out Training #
by Konrad Bałys and Sebastian Zęderowski

1. Scaling Up and Out
	* [Up vs Out](https://www.techrepublic.com/article/scaling-your-sql-server-system/)
	* [Scaling SQL Server (up and out) by Brent Ozar](https://www.brentozar.com/sql/scaling-sql-server/)
	* Scaling up or out by Brent Ozar - [Tutorial Part 1](https://www.brentozar.com/archive/2011/02/scaling-up-or-scaling-out/) and [Tutorial Part 2](https://www.brentozar.com/archive/2011/03/scaling-up-or-scaling-out-part-two/)
	* [SQL Performance trainng - mainly scaling up](https://bitbucket.org/kbalys/sqlperformance/src/master/)
1. Partitioning
	* [Pattern](https://docs.microsoft.com/en-us/azure/architecture/best-practices/data-partitioning)
4. Distributed Partitioned Views
	* [Partitioned Views](https://docs.microsoft.com/en-us/sql/t-sql/statements/create-view-transact-sql?view=sql-server-2017#additional-conditions-for-distributed-partitioned-views)
	* [Partitioned View](https://www.sqlshack.com/sql-server-partitioned-views/)
2. Sharding
	* [Sharding pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/sharding)
	* [Sharding](https://www.brentozar.com/articles/sharding/)
3. Log shipping
	* [Log Shipping](https://docs.microsoft.com/en-us/sql/database-engine/log-shipping/about-log-shipping-sql-server?view=sql-server-2017)
5. Materialized views 
	* [Materialized View Pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/materialized-view)
5. Cache
	* [Cache pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/category/performance-scalability)
6. Index table
	* [Index Table](https://docs.microsoft.com/en-us/azure/architecture/patterns/index-table)
7. Application patterns
	* [Priority Queue](https://docs.microsoft.com/en-us/azure/architecture/patterns/priority-queue)
	* [Queue based load leveling](https://docs.microsoft.com/en-us/azure/architecture/patterns/queue-based-load-leveling)
	* [Leader election](https://docs.microsoft.com/en-us/azure/architecture/patterns/leader-election)
	* [CQRS](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs)
	* [Event Sourcing](https://docs.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)
8. Linked servers
	* [Setup](https://docs.microsoft.com/en-us/sql/relational-databases/linked-servers/linked-servers-database-engine?view=sql-server-2017)
9. SQL Azure
	* [Hyperscale Azure SQL Database](https://docs.microsoft.com/en-gb/azure/sql-database/sql-database-service-tier-hyperscale)
	* [Scaling out with Azure SQL Database](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-elastic-scale-introduction)
	* [Getting Started](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-elastic-scale-get-started)
	* [Elastic queries](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-elastic-query-overview)
	* [Multitenancy and Sharding](https://docs.microsoft.com/en-us/azure/sql-database/saas-tenancy-elastic-tools-multi-tenant-row-level-security)
	* [Shard mapping](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-elastic-scale-shard-map-management)
	* [Data dependent routing](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-elastic-scale-data-dependent-routing)
10. High Availability
	* [High Availability](https://docs.microsoft.com/en-us/sql/sql-server/failover-clusters/high-availability-solutions-sql-server?view=sql-server-2017)
	* [Always On](https://docs.microsoft.com/en-us/sql/database-engine/availability-groups/windows/overview-of-always-on-availability-groups-sql-server?view=sql-server-2017)
	* [Deprecated Mirroring](https://docs.microsoft.com/en-us/sql/database-engine/database-mirroring/the-database-mirroring-endpoint-sql-server?view=sql-server-2017)
	* [Failover instances](https://docs.microsoft.com/en-us/sql/sql-server/failover-clusters/windows/always-on-failover-cluster-instances-sql-server?view=sql-server-2017)
	* [Distributed Availability Groups](https://docs.microsoft.com/en-us/sql/database-engine/availability-groups/windows/distributed-availability-groups?view=sql-server-2017)
	* [Read Scale Availability Group](https://docs.microsoft.com/en-us/sql/database-engine/availability-groups/windows/read-scale-availability-groups?view=sql-server-2017)
	* [Secondary Replica](https://docs.microsoft.com/en-us/sql/database-engine/availability-groups/windows/active-secondaries-readable-secondary-replicas-always-on-availability-groups?view=sql-server-2017)	
11. Replication
	* [Replication](https://docs.microsoft.com/en-us/sql/relational-databases/replication/sql-server-replication?view=sql-server-2017)
	* [Transactional Replication](https://docs.microsoft.com/en-us/sql/relational-databases/replication/transactional/transactional-replication?view=sql-server-2017)
12. Third party products	
13. Stretched datbaase
	* [Stretched database](https://docs.microsoft.com/en-us/sql/sql-server/stretch-database/stretch-database?view=sql-server-2017)
	